sap.ui.jsview("conceptmapper.conceptDetailPage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf conceptmapper.conceptDetailPage
	 */
	getControllerName : function() {
		return "conceptmapper.conceptDetailPage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf conceptmapper.conceptDetailPage
	 */
	createContent : function(oController) {
	

		/////// Concept DETAIL /////////////////
		var oConceptDetailPanelContent = new sap.m.ObjectListItem({
				title : "{conceptDetailModel>/concept_name}",
				number : "{conceptDetailModel>/standard_concept}",
				numberUnit : "{conceptDetailModel>/invalid_reason}",
				attributes : [ 
					new sap.m.ObjectAttribute({
					title : "{i18n>concept_id}",
					text : "{conceptDetailModel>/concept_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>concept_code}",
					text : "{conceptDetailModel>/concept_code}",
	
				}), new sap.m.ObjectAttribute({
					title : "{i18n>concept_class_id}",
					text : "{conceptDetailModel>/concept_class_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>vocabulary_id}",
					text : "{conceptDetailModel>/vocabulary_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>domain_id}",
					text : "{conceptDetailModel>/domain_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>synonyms}",
					text: "{conceptDetailModel>/synonyms}",
	
				}), 
	
				]
	
			});
		
		
		/////// Hierarchy/////////////////

		var aAncestorColumns = [
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_name}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_code}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>vocabulary_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>domain_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>standard_concept}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>validity}"
				}),
			}),		
		];
		
		var oAncestorTemplate = new sap.m.ColumnListItem({

			type: "Active",
			press: [0, oController.goToConceptDetail, oController ],
			cells : [
				new sap.m.Text({
					text : "{conceptDetailModel>concept_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_name}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_code}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>vocabulary_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>domain_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>standard_concept}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>invalid_reason}",
					wrapping : true
				}),
				
				
			]
		});
		
		
		var oAncestorHeaderToolbar = new sap.m.Toolbar({
			content : [
				new sap.m.Title({
					text : "{i18n>parents}"
				}),
			]
		});
		
		var oAncestorTable = new sap.m.Table({
			fixedLayout:false,

			growing: true,
			growingThreshold: 10,
			columns : aAncestorColumns,
			headerToolbar : oAncestorHeaderToolbar,

		});

		
		
		
		
		oAncestorTable.bindItems({
			path: "conceptDetailModel>/ancestor",
			template : oAncestorTemplate,
		});
		
		
		var aDescendantColumns = [
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_name}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_code}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>vocabulary_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>domain_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>standard_concept}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>validity}"
				}),
			}),
			

		];
		
		
		var oDescendantTemplate = new sap.m.ColumnListItem({

			type: "Active",
			press: [0, oController.goToConceptDetail, oController ],
			cells : [
				new sap.m.Text({
					text : "{conceptDetailModel>concept_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_name}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_code}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>vocabulary_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>domain_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>standard_concept}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>invalid_reason}",
					wrapping : true
				}),
				
				
			]
		});
		
		
		
		var oDescendantHeaderToolbar = new sap.m.Toolbar({
			content : [
				new sap.m.Title({
					text : "{i18n>children}"
				}),
			]
		});
		
		var oDescendantTable = new sap.m.Table({
			fixedLayout:false,

			growing: true,
			growingThreshold: 10,
			columns : aDescendantColumns,
			headerToolbar : oDescendantHeaderToolbar,

		});

		
		oDescendantTable.bindItems({
			path: "conceptDetailModel>/descendant",
			template : oDescendantTemplate,
		});
		
		
		/////// Relation /////////////////

		var aColumns = [
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>relationship_id}"
				}),
				mergeDuplicates: true
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>domain_id}"
				}),

			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>vocabulary_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_id}"
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>concept_name}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>standard_concept}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>validity}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>more_infos}"
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text : "{i18n>deletion}"
				}),
			}),
			

		];
		
		var oTemplate = new sap.m.ColumnListItem({

			type: "Active",
			press: [3, oController.goToConceptDetail, oController ],
			cells : [
				new sap.m.Text({
					text : "{conceptDetailModel>relationship_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>domain_id}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>vocabulary_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>concept_name}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>standard_concept}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptDetailModel>invalid_reason}",
					wrapping : false
				}),
				new sap.m.Button({
                    icon: "{conceptDetailModel>moreInfoIcon}",
                    text:"{conceptDetailModel>moreInfoText}",
                    enabled: "{conceptDetailModel>enabledMoreInfo}",
                    text:"{conceptDetailModel>moreInfoText}",
                    type: "{conceptDetailModel>typeMoreInfoButton}",
                    press: [ oController.moreInfos, oController]
				}),
				new sap.m.Button({
                    icon: "{conceptDetailModel>erasableIcon}",
                    enabled: "{conceptDetailModel>enabledErasable}",
                    text:"{conceptDetailModel>notErasableText}",
                    type: "{conceptDetailModel>typeErasableButton}",
                    press: [ oController.modifRelation, oController]
				}),
				
				
			]
		});
		
		
		var oRelationsTable = new sap.m.Table({
			fixedLayout:false,

			growing: true,
			growingThreshold: 10,
			columns : aColumns,
			
		});

		
		
		
		
		oRelationsTable.bindItems({
			path: "conceptDetailModel>/relations",
			template : oTemplate,
		});
		
		
		
		
	

		
		
		var oConceptDetailPanel = new sap.m.Panel({
			expandable : true,
			expanded : false,
			headerToolbar: new sap.m.Toolbar({
				content : [
					new sap.m.Title({
						text : "{i18n>concept_detail}"
					}),

					]
			}),
			content : [ oConceptDetailPanelContent ]
		});
		
		
		var oHierarchyPanel = new sap.m.Panel( {
			expandable : true,
			expanded : false,
			
			headerToolbar: new sap.m.Toolbar({
				content : [
					new sap.m.Title({
						text : "{i18n>hierarchy}"
					}),
					]
			}),
			content : [ oAncestorTable,  oDescendantTable, ]
		});
		
		
		
		
		
		var oRelationsPanel = new sap.m.Panel( {
			expandable : false,
			expanded : true,
			
			headerToolbar: new sap.m.Toolbar({
				content : [
					new sap.m.Title({
						text : "{i18n>relationship_table}"
					}),
					]
			}),
			content : [ oRelationsTable,  ]
		});

		
		
		var oSubHeader = new sap.m.Bar({
			contentMiddle : [ new sap.m.Label({
				text : "{conceptDetailModel>/concept_name}"
			}) ]
		});
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			showNavButton: true, 
			navButtonPress : [ oController.appBack, oController ], 
			showSubHeader : true,
			subHeader : oSubHeader,
			headerContent : [ 
			
				new sap.m.Button({
					text: '{loginModel>/m_username}',
					icon : "sap-icon://employee",
					enabled:true,
	
					press: [ oController.userInfos, oController ],
				}),
				new sap.m.Button({
					text: 'Log-out',
					icon : "sap-icon://decline",
					press: [ oController.logout, oController ],
				})
				
			],
			content : [ oConceptDetailPanel, oHierarchyPanel,
				oRelationsPanel ]
		});

		return oPage;
	}

});