sap.ui.define([	
	"../utils/loginAjax",
	
], function(loginAjax) {
   "use strict";

   return {
	   


	   
	   
	   
	   oAuthDialogOpen : function(oAuthDialog, oSimpleForm) {
		   
		   
		   if (oSimpleForm) {
			   
			   var content = oSimpleForm.getContent()[0];
			   
			   var oData = {};
			   $(document).unbind('keydown');

              
				
    		   $(document).keydown(function(evt){
    			   
    			   // focus on username
    			   if (content.getAggregation('items')[0].getProperty('value') === "" && 
    					   evt.keyCode !== 9 && evt.keyCode !== 16 ) { // tab and shitf
        			   content.getAggregation('items')[0].focus();
        		   // then on password
    			   } else if (content.getAggregation('items')[0].getProperty('value') !== "" &&
    					   content.getAggregation('items')[1].getProperty('value') === "" &&
    					   evt.keyCode == 13) { //enter
        			   content.getAggregation('items')[1].focus();
                   // then post
    			   } else if (content.getAggregation('items')[0].getProperty('value') !== "" &&
    					   content.getAggregation('items')[0].getProperty('value') !== "" &&
    					   evt.keyCode == 13) {
    			   
    			                      	
			            oData.m_username = content.getAggregation('items')[0].getProperty('value');
			            oData.m_password = content.getAggregation('items')[1].getProperty('value'); 
			            loginAjax.postLoginAjax(oData);
						oAuthDialog.close();

					}
    		   });
		   }
	
	
	   },
	   
	   
	   

   };
});