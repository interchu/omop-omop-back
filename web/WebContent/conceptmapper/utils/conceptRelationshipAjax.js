sap.ui.define([], function() {
   "use strict";

   return {
	   
	   
	   
	   putConceptRelationshipAjax : function (concept_id_1, oData) {
	    	sap.ui.core.BusyIndicator.show(0);

			var URL_concept_relationship = "http://localhost:5000/concept_relationship/";
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			$.ajax({
				type:"PUT",
				url: URL_concept_relationship + concept_id_1,
				dataType:"json",
				async: false,
				data: oData,
				header: {
					"Content-Type": "application/json",
				},
				success: function(data, response, xhr) {
					var ret = "Item concept_id = " + concept_id_1 +  " mapped to concept_id = " + oData.concept_id_2;
					sap.m.MessageToast.show(ret);
					console.log("Connexion : putConceptRelationshipAjax()");
				},
				beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
				error : function(error) {
					sap.m.MessageToast.show("Connexion error");
					console.log("Connexion error : putConceptRelationshipAjax()");
					console.log(error);
				}
			});
	    	sap.ui.core.BusyIndicator.hide();

		}, 
	   
  
	   deleteConceptRelationshipAjax : function (concept_id_1, oData) {
	    	sap.ui.core.BusyIndicator.show(0);

			var URL_concept_relationship = "http://localhost:5000/concept_relationship/";
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			
			$.ajax({
				type:"DELETE",
				url: URL_concept_relationship + concept_id_1,
				dataType:"json",
				async: false,
				data: oData,
				header: {
					"Content-Type": "application/json",
				},
				success: function(data, response, xhr) {
					var ret = "Delete relation beetween concept_id = " + concept_id_1 + " and concept_id = " + oData.concept_id_2;
					sap.m.MessageToast.show(ret);
					console.log("Connexion : deleteConceptRelationshipAjax()");
				},
				beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
				error : function(error) {	
					sap.m.MessageToast.show("Connexion error");
					console.log("Connexion error : deleteConceptRelationshipAjax()");
					console.log(error);
				}
			});
			
	    	sap.ui.core.BusyIndicator.hide();

		}



	   
	   
   };
});