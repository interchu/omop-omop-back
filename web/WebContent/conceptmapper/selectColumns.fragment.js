sap.ui.jsfragment("conceptmapper.selectColumns", {
	
	createContent: function(oController) {

		var oColumsDialog = new sap.m.Dialog({
			title : "{i18n>select_columns}",
			icon : "sap-icon://customize",
		});
		
		oColumsDialog.addContent(
				new sap.m.List({
					id: "selectColumnsList",
					  mode: sap.m.ListMode.MultiSelect, 
					  setGrowingScrollToLoad: true,
					  items : [
						  
						  	new sap.m.StandardListItem({
								title: "{i18n>concept_id}",
								selected: true,
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_name}",
								selected: true,
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_code}",
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_class_id}",
							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>vocabulary_id}",
								selected: true,
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>domain_id}",
								selected: true,
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>standard_concept}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>validity}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>frequency}",
							}),
						  
							
							
					  ]
			
					  
					})
		);
		
		oColumsDialog.addButton(
                new sap.m.Button({
                    text: "OK",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,

					
                    press: function () {
                    	var columnsList = sap.ui.getCore().byId("selectColumnsList");
                    	sap.ui.getCore().byId("conceptIdSearch").setVisible(columnsList.getAggregation('items')[0].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptNameSearch").setVisible(columnsList.getAggregation('items')[1].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptCodeSearch").setVisible(columnsList.getAggregation('items')[2].getProperty('selected'));
                    	sap.ui.getCore().byId("conceptClassSearch").setVisible(columnsList.getAggregation('items')[3].getProperty('selected'));
                    	sap.ui.getCore().byId("vocabularySearch").setVisible(columnsList.getAggregation('items')[4].getProperty('selected'));
                    	sap.ui.getCore().byId("standardSearch").setVisible(columnsList.getAggregation('items')[6].getProperty('selected'));
                    	sap.ui.getCore().byId("validitySearch").setVisible(columnsList.getAggregation('items')[7].getProperty('selected'));
                    	sap.ui.getCore().byId("domainSearch").setVisible(columnsList.getAggregation('items')[5].getProperty('selected'));
                    	sap.ui.getCore().byId("frequencySearch").setVisible(columnsList.getAggregation('items')[8].getProperty('selected'));

                    	oColumsDialog.close();
					}
                })
        );

		
		
		return oColumsDialog;
		
	}
});