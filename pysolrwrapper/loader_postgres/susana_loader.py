from __future__ import annotations

import logging
from enum import Enum
from pathlib import Path

from pandas import DataFrame, read_csv, read_sql

from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient


class PerimeterEnum(str, Enum):
    CONCEPT = "CONCEPT"
    RELATIONSHIP = "RELATIONSHIP"
    ALL = "ALL"


class SusanaLoader:
    DELIMITER = ","
    HEADER = 0  # first line

    def __init__(
        self,
        pg_client: PostgresClient,
        m_project_type_id: str,
        m_user_id: int,
        domain_id: str,
        m_language_id: str,
        vocabulary_id: str,
        concept_path: Path = None,
        relationship_path: Path = None,
        debug=False,
    ):
        """Loader Susana Class"""
        self.pg_client = pg_client
        self.m_project_type_id = m_project_type_id
        self.domain_id = domain_id
        self.m_user_id = m_user_id
        self.m_language_id = m_language_id
        self.vocabulary_id = vocabulary_id
        self.concept_path = concept_path
        self.relationship_path = relationship_path
        self.vocabulary_ids = read_sql("select vocabulary_id from vocabulary", pg_client.conn)[
            "vocabulary_id"
        ].tolist()  # TODO: fetch from vocabularies
        self.debug = debug
        logging.info("Existing vocabularies %s", self.vocabulary_ids)

    def create_tmp(self) -> SusanaLoader:
        logging.info("Creating OMOP table...")
        sql = """
        CREATE UNLOGGED TABLE concept_tmp(
            concept_name text,
            concept_code text NOT NULL,
            m_language_id text,
            m_project_type_id text,
            domain_id text,
            vocabulary_id text,
            m_statistic_type_id text,
            m_value_as_number double precision,
            concept_class_id text);

        CREATE UNLOGGED TABLE concept_relationship_tmp(
            concept_code text NOT NULL,
            vocabulary_id_target text NOT NULL,
            concept_code_target text NOT NULL,
            vocabulary_id text NULL,
            relationship_id text NULL);

        CREATE UNLOGGED TABLE vocabulary_tmp(
            vocabulary_id text NOT NULL,
            vocabulary_name text NOT NULL,
            vocabulary_reference text NULL,
            vocabulary_version text NULL);

        CREATE UNLOGGED TABLE concept_synonym_tmp(
            concept_code text NOT NULL,
            concept_synonym_name text NOT NULL,
            language_concept_id bigint NOT NULL);
        """
        self.pg_client.run(sql)
        return self

    def purge_omop(self) -> SusanaLoader:
        logging.info("Purging OMOP table...")
        sql = f"""
        WITH del AS (
            DELETE FROM concept
            WHERE lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                    AND domain_id IN ('{self.domain_id}', 'Metadata')
                    AND m_project_id IN (
                        SELECT
                            m_project_id
                        FROM
                            mapper_project
                        WHERE
                            lower(m_project_type_id) = lower('{self.m_project_type_id}'))
                    RETURNING
                        concept_id
        ),
        del_statistic AS (
            DELETE FROM mapper_statistic
            WHERE m_concept_id IN (
                    SELECT
                        concept_id
                    FROM
                        del)
        ),
        del_synonym AS (
            DELETE FROM concept_synonym
            WHERE concept_id IN (
                    SELECT
                        concept_id
                    FROM
                        del)
        ),
        del_vocabulary AS (
            DELETE FROM vocabulary
            WHERE lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                RETURNING
                    vocabulary_concept_id
        ),
        del_voc_concept AS (
            DELETE FROM concept
            WHERE concept_id IN (
                    SELECT
                        vocabulary_concept_id
                    FROM
                        del_vocabulary)
        ),
        delete_rel1 AS (
            DELETE FROM concept_relationship
            WHERE concept_id_1 IN (
                    SELECT
                        concept_id
                    FROM
                        del))
        DELETE FROM concept_relationship
        WHERE concept_id_2 IN (
                SELECT
                    concept_id
                FROM
                    del)
        """
        self.pg_client.run(sql)
        return self

    def refresh_omop(self) -> SusanaLoader:
        sql = """
        WITH allc AS (
            SELECT
                concept_name,
                coalesce(concept_code, 'UNKNOWN') concept_code,
                m_language_id,
                coalesce(m_project_id, 1) AS m_project_id,
                domain_id,
                vocabulary_id,
                now()::date valid_start_date,
                '20990101'::date valid_end_date,
                concept_tmp.concept_class_id
            FROM
                concept_tmp
                LEFT JOIN mapper_project
                ON lower(concept_tmp.m_project_type_id)
                    = lower(mapper_project.m_project_type_id)
        ),
        ins AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                a.concept_name,
                a.concept_code,
                a.m_language_id,
                a.m_project_id,
                a.domain_id,
                a.vocabulary_id,
                a.valid_start_date,
                a.valid_end_date,
                a.concept_class_id
            FROM
                allc a
                LEFT JOIN concept ON a.vocabulary_id = concept.vocabulary_id
                    AND a.concept_code = concept.concept_code
            WHERE
                concept.vocabulary_id IS NULL
        ),
        upt AS (
            UPDATE
                concept
            SET
                concept_name = a.concept_name,
                concept_code = a.concept_code,
                m_language_id = a.m_language_id,
                m_project_id = a.m_project_id,
                domain_id = a.domain_id,
                vocabulary_id = a.vocabulary_id,
                valid_start_date = a.valid_start_date,
                valid_end_date = a.valid_end_date,
                concept_class_id = a.concept_class_id
            FROM
                allc a
            WHERE
                concept.vocabulary_id = a.vocabulary_id
                AND concept.concept_code = a.concept_code)
            , del as (
            UPDATE concept SET valid_end_date = now()
            WHERE
            vocabulary_id IN (select vocabulary_id from concept_tmp)
            AND NOT EXISTS (SELECT 1
                      FROM concept_tmp
                      WHERE concept_tmp.vocabulary_id = concept.vocabulary_id
                            AND concept_tmp.concept_code = concept.concept_code
             )
            )
            INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
            SELECT
                concept.concept_id,
                'MAPPING',
                0,
                0
            FROM
                concept_tmp
                JOIN concept ON (concept.concept_code = concept_tmp.concept_code
                        AND concept_tmp.vocabulary_id = concept.vocabulary_id)
            UNION ALL
            SELECT
                concept.concept_id,
                'BULK_TRANSLATION',
                0,
                0
            FROM
                concept_tmp
                JOIN concept ON (concept.concept_code = concept_tmp.concept_code
                        AND concept_tmp.vocabulary_id = concept.vocabulary_id)
        """
        self.pg_client.run(sql)
        return self

    def refresh_omop_relationship(self) -> SusanaLoader:
        sql = """
        WITH ins_relationship_to AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
         valid_start_date, valid_end_date, m_user_id)
            SELECT
                c1.concept_id,
                c.concept_id,
                r.relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                concept_relationship_tmp r
                JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
                JOIN concept c1 ON (lower(c1.concept_code) = lower(r.concept_code)
                        AND lower(c1.vocabulary_id) = lower(r.vocabulary_id))
                LEFT JOIN concept_relationship c_r ON (c_r.concept_id_1 = c1.concept_id
                        AND c_r.concept_id_2 = c.concept_id
                        AND lower(c_r.relationship_id) = lower(r.relationship_id))
            WHERE
                c_r.concept_id_1 IS NULL
            RETURNING
                concept_id_1
        ),
        ins_relationship_from AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
        valid_start_date, valid_end_date, m_user_id)
            SELECT
                c.concept_id,
                c1.concept_id,
                rel.reverse_relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                concept_relationship_tmp r
                JOIN relationship rel ON (rel.relationship_id = r.relationship_id)
                JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
                JOIN concept c1 ON (lower(c1.concept_code) = lower(r.concept_code)
                        AND lower(c1.vocabulary_id) = lower(r.vocabulary_id))
                LEFT JOIN concept_relationship c_r ON (c_r.concept_id_1 = c.concept_id
                        AND c_r.concept_id_2 = c1.concept_id
                        AND c_r.relationship_id = rel.relationship_id)
            WHERE
                c_r.concept_id_1 IS NULL
            RETURNING
                concept_id_1)
        INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
        SELECT
            concept_id_1,
            'MAPPING',
            0,
            0
        FROM
            ins_relationship_to
        UNION ALL
        SELECT
            concept_id_1,
            'MAPPING',
            0,
            0
        FROM
            ins_relationship_from
        """
        self.pg_client.run(sql)
        return self

    def load_omop(self) -> SusanaLoader:
        logging.info("Loading OMOP table...")
        sql = f"""
        WITH ins AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                concept_name,
                coalesce(concept_code, 'UNKNOWN'),
                m_language_id,
                coalesce(m_project_id, 1) AS m_project_id,
                domain_id,
                vocabulary_id,
                now()::date,
                '20990101',
                concept_tmp.concept_class_id
            FROM
                concept_tmp
                LEFT JOIN mapper_project ON lower(concept_tmp.m_project_type_id)
                                          = lower(mapper_project.m_project_type_id)
            RETURNING
                concept_id,
                concept_code,
                vocabulary_id
        ),
        voc_cpt AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                vocabulary_name,
                '{self.m_project_type_id}' || ' ' || 'generated',
                '{self.m_language_id}',
                coalesce(m_project_id, 0) AS m_project_id,
                'Metadata',
                '{self.m_project_type_id} - Vocabulary',
                now()::date,
                '20990101',
                'Vocabulary'
            FROM
                vocabulary_tmp
                LEFT JOIN mapper_project ON lower('{self.m_project_type_id}')
                                          = lower(mapper_project.m_project_type_id)
            WHERE
                lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                RETURNING
                    concept_id,
                    concept_name,
                    m_project_id
        ),
        ins_vocabulary AS (
        INSERT INTO vocabulary (vocabulary_id, vocabulary_name, vocabulary_reference,
                                vocabulary_version, vocabulary_concept_id, m_project_id)
            SELECT
                vocabulary_id,
                vocabulary_name,
                vocabulary_reference,
                vocabulary_version,
                voc_cpt.concept_id,
                voc_cpt.m_project_id
            FROM
                vocabulary_tmp
                JOIN voc_cpt ON voc_cpt.concept_name = vocabulary_tmp.vocabulary_name
            WHERE
                lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
        ),
        ins_statistic AS (
        INSERT INTO mapper_statistic (m_concept_id, m_statistic_type_id, m_value_as_number,
                                      m_algo_id, m_user_id, m_valid_start_datetime)
            SELECT
                concept_id,
                m_statistic_type_id,
                m_value_as_number,
                4,
                10,
                now()
            FROM
                ins
                JOIN concept_tmp USING (concept_code)
            WHERE
                m_value_as_number IS NOT NULL
        ),
        ins_relationship_to AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
                                          valid_start_date, valid_end_date, m_user_id)
            SELECT
                ins.concept_id,
                c.concept_id,
                r.relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                ins
                JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
                JOIN (SELECT concept_id, concept_code, vocabulary_id
                FROM ins
                UNION ALL
                SELECT concept_id, concept_code, vocabulary_id
                FROM concept) c
                ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
        ),
        ins_relationship_from AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
                                          valid_start_date, valid_end_date, m_user_id)
            SELECT
                c.concept_id,
                ins.concept_id,
                rel.reverse_relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                ins
            JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
            JOIN relationship rel ON (rel.relationship_id = r.relationship_id)
            JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                    AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
        ),
        ins_job AS (
        INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
            SELECT
                concept_id,
                'MAPPING',
                0,
                0
            FROM
                ins)
            INSERT INTO concept_synonym (concept_id, concept_synonym_name,
                                           language_concept_id, m_user_id)
            SELECT
                ins.concept_id,
                concept_synonym_name,
                t.language_concept_id,
                0
            FROM
                concept_synonym_tmp t
            JOIN ins ON (ins.concept_code = t.concept_code)
        """
        self.pg_client.run(sql)
        return self

    def drop_tmp(self) -> SusanaLoader:
        logging.info("Droping OMOP table...")
        sql = """
        DROP TABLE IF EXISTS concept_tmp;
        DROP TABLE IF EXISTS concept_relationship_tmp;
        DROP TABLE IF EXISTS vocabulary_tmp;
        DROP TABLE IF EXISTS concept_synonym_tmp;
        """
        self.pg_client.run(sql)
        return self

    def load_tmp(self) -> None:
        logging.info("Loading OMOP tmp table...")
        pd_concept = (
            read_csv(
                str(self.concept_path),
                delimiter=self.DELIMITER,
                header=self.HEADER,
            )
            if self.concept_path
            else None
        )
        pd_relation = (
            read_csv(
                str(self.relationship_path),
                delimiter=self.DELIMITER,
                header=self.HEADER,
            )
            if self.relationship_path
            else None
        )
        errors = 0
        res_concept = []
        if pd_concept is not None:
            res_concept = SusanaLoader.validate_concept(pd_concept, self.vocabulary_id)
            if not all(res.success for res in res_concept):
                errors += 1
                logging.error(res_concept)

        res_relation = []
        if pd_relation is not None:
            res_relation = SusanaLoader.validate_relation(pd_relation, self.vocabulary_id, self.vocabulary_ids)
            if not all(res.success for res in res_relation):
                errors += 1
                logging.error(res_relation)

        if bool(errors):
            raise ValueError("Not properly formatted csv %s %s", res_concept, res_relation)

        pd_vocabulary = DataFrame(
            {
                "vocabulary_id": [self.vocabulary_id],
                "vocabulary_name": ["foo"],
                "vocabulary_reference": ["bar"],
                "vocabulary_version": [""],
            }
        )
        self.pg_client.bulk_load_pandas(df=pd_vocabulary, table="vocabulary_tmp")
        if pd_relation is not None:
            self.pg_client.bulk_load_pandas(df=pd_relation, table="concept_relationship_tmp")
        if pd_concept is not None:
            pd_concept["domain_id"] = self.domain_id
            pd_concept["vocabulary_id"] = self.vocabulary_id
            pd_concept["m_project_type_id"] = self.m_project_type_id
            self.pg_client.bulk_load_pandas(df=pd_concept, table="concept_tmp")

    @staticmethod
    def validate_concept(concept: DataFrame, vocabulary_id):
        logging.info("Validating concepts...")
        col_concept = ["concept_name", "concept_code", "vocabulary_id"]
        col_not_null = [
            {
                "expectation_type": "expect_column_values_to_not_be_null",
                "kwargs": {"column": col},
            }
            for col in col_concept
        ]
        col_exist = [
            {
                "expectation_type": "expect_column_to_exist",
                "kwargs": {"column": col},
            }
            for col in col_concept
        ]
        exp_concept = {
            "expectation_suite_name": "foo",
            "expectations": [
                {
                    "expectation_type": "expect_column_values_to_be_unique",
                    "kwargs": {"column": "concept_code"},
                },
                {
                    "expectation_type": "expect_column_values_to_be_in_set",
                    "kwargs": {
                        "column": "vocabulary_id",
                        "value_set": [vocabulary_id],
                    },
                },
            ]
            + col_exist
            + col_not_null,
        }
        from great_expectations.dataset import PandasDataset

        ge_concept = PandasDataset(concept, expectation_suite=exp_concept)
        return ge_concept.validate().results

    @staticmethod
    def validate_relation(relation: DataFrame, vocabulary_id, vocabulary_ids):
        logging.info("Validating relationship...")
        col_relation = [
            "concept_code",
            "concept_code_target",
            "vocabulary_id",
            "vocabulary_id_target",
            "relationship_id",
        ]

        col_not_null = [
            {
                "expectation_type": "expect_column_values_to_not_be_null",
                "kwargs": {"column": col},
            }
            for col in col_relation
        ]

        col_exist = [
            {
                "expectation_type": "expect_column_to_exist",
                "kwargs": {"column": col},
            }
            for col in col_relation
        ]

        exp_relationship = {
            "expectation_suite_name": "foo",
            "expectations": [
                {
                    "expectation_type": "expect_compound_columns_to_be_unique",
                    "kwargs": {"column_list": col_relation},
                },
                {
                    "expectation_type": "expect_column_values_to_be_in_set",
                    "kwargs": {
                        "column": "vocabulary_id",
                        "value_set": [vocabulary_id],
                    },
                },
                {
                    "expectation_type": "expect_column_values_to_be_in_set",
                    "kwargs": {
                        "column": "vocabulary_id_target",
                        "value_set": vocabulary_ids + [vocabulary_id],
                    },
                },
            ]
            + col_exist
            + col_not_null,
        }

        from great_expectations.dataset import PandasDataset

        ge_relationship = PandasDataset(relation, expectation_suite=exp_relationship)
        return ge_relationship.validate().results

    def run(self):
        logging.info("Loading OMOP...")
        try:
            self.drop_tmp()
            self.create_tmp()
            self.load_tmp()
            self.purge_omop()
            self.load_omop()
        finally:
            if not self.debug:
                self.drop_tmp()

    def refresh(self, perimeter) -> None:
        try:
            self.drop_tmp()
            self.create_tmp()
            self.load_tmp()
            if perimeter in (PerimeterEnum.CONCEPT, PerimeterEnum.ALL):
                self.refresh_omop()
            if perimeter in (PerimeterEnum.RELATIONSHIP, PerimeterEnum.ALL):
                self.refresh_omop_relationship()
        finally:
            if not self.debug:
                self.drop_tmp()
