import logging
from pathlib import Path

import aiosql
import click

from conf_files.config import POSTGRES
from pysolrwrapper.loader_postgres.postgres_utils import (
    PostgresClient,
    PostgresConf,
    rename,
)
from pysolrwrapper.utils import resource_path_root


@click.command()
@click.option(
    "--athena-folder",
    type=click.Path(exists=True),
    help="Folder where athena download have been unzipped",
    required=True,
)
def upgrade(athena_folder):
    """Upgrade the susana concepts with a new athena export"""
    pg_client = PostgresClient.from_conf(PostgresConf(**POSTGRES))
    conn = pg_client.conn
    upgrade_concept(conn, Path(athena_folder))


def upgrade_concept(conn, athena_path: Path):
    with conn.cursor() as curs:
        curs.execute("set search_path to susana")
    data_path = resource_path_root() / "postgresql/queries"
    queries = aiosql.from_path(str(data_path), "psycopg2")
    logging.info("begin transaction")
    queries.athena.begin_transaction(conn)
    logging.info("drop temporary tables")
    queries.athena.drop_concept_table(conn)
    logging.info("create temporary tables")
    queries.athena.create_concept_table(conn)
    queries.athena.create_job_table(conn)
    logging.info("load csv data")
    load_csv(conn, athena_path)

    queries.athena.deactivate_foreign_key(conn)

    logging.info("insert concept")
    queries.athena.insert_concept(conn)
    queries.athena.update_concept(conn)

    logging.info("insert vocabulary")
    queries.athena.insert_vocabulary(conn)
    queries.athena.update_vocabulary(conn)

    logging.info("insert relationship")
    queries.athena.insert_relationship(conn)
    queries.athena.update_relationship(conn)

    logging.info("insert domain")
    queries.athena.insert_domain(conn)
    queries.athena.update_domain(conn)

    logging.info("insert concept_class")
    queries.athena.insert_concept_class(conn)
    queries.athena.update_concept_class(conn)

    logging.info("insert concept_synonym")
    queries.athena.insert_concept_synonym(conn)
    #queries.athena.update_concept_synonym(conn)

    logging.info("insert concept_ancestor")
    queries.athena.insert_concept_ancestor(conn)
    queries.athena.update_concept_ancestor(conn)

    logging.info("insert concept_relationship")
    queries.athena.insert_concept_relationship(conn)
    queries.athena.update_concept_relationship(conn)

    queries.athena.reactivate_foreign_key(conn)

    logging.info("insert job")
    queries.athena.insert_job(conn)
    logging.info("drop temporary tables")
    queries.athena.drop_concept_table(conn)
    logging.info("commit transaction")
    queries.athena.commit_transaction(conn)

    return queries.athena.test(conn)


def load_csv(conn, athena_path: Path):
    pg_client = PostgresClient(conn)
    csvs = athena_path.glob("*csv")
    sorted_csvs = sorted(csvs)
    list(
        map(
            lambda csv: pg_client.bulk_load(
                "tmp_" + rename(str(csv.stem)).lower(),
                str(csv),
                delimiter="\t",
                quote="\b",
            ),
            sorted_csvs,
        )
    )
