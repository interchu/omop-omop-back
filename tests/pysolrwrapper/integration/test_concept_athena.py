from pysolrwrapper.loader_postgres.upgrade_concept import upgrade_concept
from pysolrwrapper.utils import resource_path_root


def test_load_athena(containers):
    data_path = resource_path_root() / "postgresql/athena"
    a = upgrade_concept(containers.pg_client.conn, data_path)
    print(a)
