import json

from pysolrwrapper.core import SolrQuery


def test_post_login(client):
    json_data = """
    {
    "m_username": "admin",
    "m_password": "toto"
    }
      """
    dict_data = json.loads(json_data)
    result = client.post("login", json=dict_data)
    result_dict = result.json

    assert result.status_code == 200
    assert result_dict["m_is_admin"] == "Admin"


def test_get_concept(client):
    result = client.get(
        "concept/2002025460", headers={"Authorization": "Bearer fake-token"}
    )
    goal_dict = {
        "concept_id": 2002025460,
        "concept_name": "Paramethadione",
        "concept_code": "N03AC01",
        "concept_class_id": "EMPTY",
        "vocabulary_id": "OSIRIS - ATC",
        "domain_id": "Drug",
        "invalid_reason": "Valid",
        "standard_concept": "Non-Standard",
        "m_language_id": "EN",
        "m_project_id": 6,
        "m_frequency_id": 0,
        "synonyms": "",
        "relations": None,
        "ancestor": [],
        "descendant": [],
    }

    assert result.status_code == 200
    assert result.json == goal_dict


def test_put_concept_relationship(containers, client):
    dict_data = {
        "m_mapping_rate": 0,
        "m_mapping_comment": None,
        "concept_id_2": 2002025405,
        "relationship_id": "Maps to",
        "helper": None,
    }
    result = client.put(
        "concept_relationship/2002025457",
        json=dict_data,
        headers={"Authorization": "Bearer fake-token"},
    )
    assert result.status_code == 201  # created

    result_get = client.get(
        "concept/2002025457", headers={"Authorization": "Bearer fake-token"}
    )
    assert len(result_get.json["relations"]) == 1

    psolr = SolrQuery.from_conf(solr_conf=containers.solr_conf).solr
    result_solr = psolr.search(
        f'concept_id:2002025457 AND is_mapped:"OSIRIS - S" AND local_map_number:1'
    )
    assert result_solr["response"]["numFound"] == 1


def test_delete_concept_relationship(containers, client):
    # Given
    dict_data = {
        "m_mapping_rate": 0,
        "m_mapping_comment": None,
        "concept_id_2": 2002025430,
        "relationship_id": "Maps to",
        "helper": None,
    }
    client.put(
        "concept_relationship/2002025408",
        json=dict_data,
        headers={"Authorization": "Bearer fake-token"},
    )
    # When
    dict_data = {
        "concept_id_2": 2002025430,
        "relationship_id": "Maps to",
    }
    result = client.delete(
        "concept_relationship/2002025408",
        json=dict_data,
        headers={"Authorization": "Bearer fake-token"},
    )
    assert result.status_code == 201  # deleted

    psolr = SolrQuery.from_conf(solr_conf=containers.solr_conf).solr
    result_solr = psolr.search(
        f'concept_id:2002025408 AND is_mapped:"OSIRIS - EMPTY" AND local_map_number:0'
    )
    assert result_solr["response"]["numFound"] == 1
