-- name: insert_domain!
insert into domain (
  domain_id        ,
  domain_name      ,
  domain_concept_id
)
select
  t.domain_id			,
  t.domain_name		,
  t.domain_concept_id
from tmp_domain as t
left join domain v on v.domain_id = t.domain_id
where v.domain_id is null;

-- name: update_domain!
update domain set
domain_name	      = v.domain_name		,
domain_concept_id = v.domain_concept_id
from tmp_domain v
where domain.domain_id = v.domain_id
and (
domain.domain_name	      is distinct from v.domain_name		or
domain.domain_concept_id is distinct from v.domain_concept_id
);
