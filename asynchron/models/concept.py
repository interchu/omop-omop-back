from datetime import datetime, timedelta

from sqlalchemy import Column, Date, Integer, Text

from asynchron.db import Base, session
from asynchron.models.concept_relationship import ConceptRelationModel
from asynchron.models.vocabulary import VocabularyModel


class ConceptModel(Base):
    __tablename__ = "concept"
    __table_args__ = {"schema": "susana"}

    concept_id = Column(
        Integer,
        primary_key=True,
    )

    concept_name = Column(Text())
    domain_id = Column(Text())
    vocabulary_id = Column(Text())
    concept_class_id = Column(Text())
    standard_concept = Column(Text())
    concept_code = Column(Text())
    valid_start_date = Column(Date(), default=datetime.now())  # CURRENT_DATE
    valid_end_date = Column(Date(), default=datetime.now() + timedelta(weeks=5200))
    invalid_reason = Column(Text())
    m_language_id = Column(Text())
    m_project_id = Column(Integer)

    def __init__(
        self,
        concept_name,
        domain_id,
        m_language_id,
        vocabulary_id,
        concept_code,
        invalid_reason=None,
        standard_concept=None,
        valid_end_date=None,
        concept_class_id="",
        m_project_id=1,
    ):
        self.concept_name = concept_name
        self.domain_id = domain_id
        self.vocabulary_id = vocabulary_id
        self.concept_class_id = concept_class_id
        self.standard_concept = standard_concept
        self.concept_code = concept_code
        if valid_end_date != None:
            self.valid_end_date = valid_end_date
        self.invalid_reason = invalid_reason
        self.m_language_id = m_language_id
        self.m_project_id = m_project_id

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from_db(self):
        session.delete(self)
        session.commit()

    @classmethod
    def find_by_id(cls, concept_id):
        return session.query(cls).filter_by(concept_id=concept_id).first()

    @classmethod
    def find_by_project_id(cls, m_project_id):
        return session.query(cls).filter_by(m_project_id=m_project_id).all()

    @classmethod
    def find_distinct_vocabulary(cls, m_project_id):
        return (
            session.query(cls)
            .distinct(cls.vocabulary_id)
            .filter_by(m_project_id=m_project_id)
            .all()
        )

    @classmethod
    def find_by_concept_code(cls, concept_code, vocabulary_id=None):
        if vocabulary_id != None:
            return (
                session.query(cls)
                .filter(
                    cls.concept_code.ilike(concept_code),
                    cls.vocabulary_id.ilike(vocabulary_id),
                )
                .all()
            )
        return session.query(cls).filter(cls.concept_code.ilike(concept_code)).all()

    @classmethod
    def find_by_voc(cls, vocabulary_id):
        return session.query(cls).filter(cls.vocabulary_id.ilike(vocabulary_id)).all()

    def similar_voc(self):
        # find the voc of the concept
        voc = VocabularyModel.find_by_id(self.vocabulary_id)
        # find the related voc
        related_concept = ConceptRelationModel.find_by_id(
            voc.vocabulary_concept_id, relationship_id="Maps to"
        )
        related_vocabulary = [
            VocabularyModel.find_by_concept_id(r.concept_id_2) for r in related_concept
        ]
        return [v.vocabulary_id for v in related_vocabulary]

    @classmethod
    def find_concept_in_similar_voc(cls, concept_id):
        concept = cls.find_by_id(concept_id)
        print("concept_name", concept.vocabulary_id)

        ret_list = []
        print("similar_voc", concept.similar_voc())
        for voc in concept.similar_voc():
            ret_list += cls.find_by_concept_code(concept.concept_code, voc)

        return ret_list
