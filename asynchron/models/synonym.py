from datetime import datetime, timedelta

from sqlalchemy import TIMESTAMP, Column, Date, Integer, Text, and_

from asynchron.db import Base, session


class SynonymModel(Base):
    __tablename__ = "concept_synonym"
    __table_args__ = {"schema": "susana"}

    concept_id = Column(
        Integer,
        primary_key=True,
    )
    concept_synonym_name = Column(
        Text(),
        primary_key=True,
    )
    language_concept_id = Column(
        Integer,
        primary_key=True,
    )
    m_valid_start_datetime = Column(Date(), default=datetime.now())  # CURRENT_DATE
    m_valid_end_datetime = Column(Date(), default=datetime.now() + timedelta(weeks=5200))
    m_invalid_reason = Column(Text())
    m_user_id = Column(Integer)
    m_algo_id = Column(Integer, primary_key=True)
    m_modif_start_datetime = Column(TIMESTAMP(), default=datetime.now())
    m_modif_end_datetime = Column(TIMESTAMP(), default=None)

    # m_language_id=4180186=='English language'
    def __init__(
        self,
        concept_id,
        concept_synonym_name,
        m_user_id,
        m_algo_id,
        language_concept_id=4180186,
        m_invalid_reason=None,
        m_modif_end_datetime=None,
    ):
        self.concept_id = concept_id
        self.concept_synonym_name = concept_synonym_name
        self.language_concept_id = language_concept_id
        self.m_invalid_reason = m_invalid_reason
        self.m_user_id = m_user_id
        self.m_algo_id = m_algo_id
        self.m_modif_end_datetime = m_modif_end_datetime

    def json(self):
        return {
            "concept_id": self.concept_id,
            "concept_synonym_name": self.concept_synonym_name,
            "language_concept_id": self.language_concept_id,
            "m_invalid_reason": self.m_invalid_reason,
            "m_user_id": self.m_user_id,
            "m_algo_id": self.m_algo_id,
        }

    @classmethod
    def find_by_id(cls, concept_id):
        return cls.query.filter_by(concept_id=concept_id).all()

    @classmethod
    def find_synonym(
        cls,
        concept_id,
        concept_synonym_name=None,
        language_concept_id=None,
        m_algo_id=None,
    ):
        # synonym and language
        if concept_synonym_name and language_concept_id and not m_algo_id:
            return (
                session.query(cls)
                .filter_by(
                    concept_id=concept_id,
                    concept_synonym_name=concept_synonym_name,
                    language_concept_id=language_concept_id,
                )
                .first()
            )

        # language and algo
        elif not concept_synonym_name and language_concept_id and m_algo_id:
            return (
                session.query(cls)
                .filter(
                    and_(
                        cls.concept_id == concept_id,
                        cls.language_concept_id == language_concept_id,
                        cls.m_algo_id.in_(m_algo_id),
                    )
                )
                .first()
            )

        # only algo_id
        elif not concept_synonym_name and not language_concept_id and m_algo_id:
            return (
                session.query(cls)
                .filter(and_(cls.concept_id == concept_id, cls.m_algo_id.in_(m_algo_id)))
                .all()
            )

        # only languague_concept_id
        elif not concept_synonym_name and language_concept_id and not m_algo_id:
            return (
                session.query(cls)
                .filter_by(concept_id=concept_id, language_concept_id=language_concept_id)
                .all()
            )

        print("SynonymModel.find_synonym abnormal")

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from(self):
        session.delete(self)
        session.commit()
