from datetime import datetime

from asynchron.models.concept import ConceptModel
from asynchron.models.concept_relationship import ConceptRelationModel
from asynchron.models.job import JobModel
from asynchron.models.project import ProjectModel
from asynchron.models.relationship import RelationshipModel
from share.macros import SEPARATOR, VOCABULARY_TO_LINK, Algo

# map
# - concept_id_1 = 2000000001 to concept_id_2 = 3433232
# ou
# - concept_id_1 - 3433232 to concept_id_2 2000000001
#
# si 200000001 related to 2000000002
# and 3433232 to 4332333
#
# 2000000001 to 3433232 #manual
# 2000000001 to 4332333 #extend
# 2000000002 to 4332233 #extend
# 2000000002 to 3433232 #extend


# relationship sender (for one m_concept_relationship_id)
def extend_relations_for_one_concept():
    # take last relations
    # base from on concept_id
    # => search relative_concept_id ie same vocabulary_id ('APHP - ccam' and 'LILLE - ccam') and same concept_code
    # => check if synonyms for the concept
    # => catch synonym for this concept_id

    result = JobModel.update_mapper_job(
        initial_status=[0, 1, 3],
        dest_status=1,
        job_type=["EXTEND_RELATIONS_TO_OTHER_PROJECT"],
    )
    print(len(result))

    if len(result) > 0:

        for m_concept_relationship_id, m_user_id in result:

            source_relation = ConceptRelationModel.find_by_concept_relationship_id(
                m_concept_relationship_id
            )

            if source_relation:
                # find concept_id with JOIN on voc and concept_code
                relative_concepts_1 = ConceptModel.find_concept_in_similar_voc(
                    source_relation.concept_id_1
                )  # return list
                relative_ids_1 = [c.concept_id for c in relative_concepts_1]
                relative_ids_1 += [source_relation.concept_id_1]
                relative_concepts_2 = ConceptModel.find_concept_in_similar_voc(
                    source_relation.concept_id_2
                )  # return list
                relative_ids_2 = [c.concept_id for c in relative_concepts_2]
                relative_ids_2 += [source_relation.concept_id_2]

                if len(relative_ids_1) > 1 or len(relative_ids_2) > 1:
                    print("relative_ids_1 : ", relative_ids_1)
                    print("relative_ids_2 : ", relative_ids_2)
                    exit()
                    for relative_id_1 in relative_ids_1:
                        for relative_id_2 in relative_ids_2:

                            if not (
                                relative_id_1 == source_relation.concept_id_1
                                and relative_id_2 == source_relation.concept_id_2
                            ):
                                relations = ConceptRelationModel.find_by_id(
                                    relative_id_1,
                                    relative_id_2,
                                    "Maps to",
                                    m_algo_id=[
                                        Algo.MANUAL_EVALUATION_MAPPING.value,
                                        Algo.FROM_OTHER_PROJECTS_MAPPING.value,
                                    ],
                                )
                                reverse_relation_name = RelationshipModel.find_by_id(
                                    source_relation.relationship_id
                                ).reverse_relationship_id

                                if relations:
                                    relations[0].m_modif_end_datetime = datetime.now()

                                    reverse = ConceptRelationModel.find_by_id(
                                        relative_id_2,
                                        relative_id_1,
                                        reverse_relation_name,
                                        m_algo_id=[
                                            Algo.MANUAL_EVALUATION_MAPPING.value,
                                            Algo.FROM_OTHER_PROJECTS_MAPPING.value,
                                        ],
                                    )
                                    if reverse:
                                        reverse[0].m_modif_end_datetime = datetime.now()

                                relation = ConceptRelationModel(
                                    concept_id_1=relative_id_1,
                                    concept_id_2=relative_id_2,
                                    relationship_id=source_relation.relationship_id,
                                    invalid_reason=source_relation.invalid_reason,
                                    m_user_id=source_relation.m_user_id,
                                    m_algo_id=Algo.FROM_OTHER_PROJECTS_MAPPING.value,
                                    m_mapping_rate=source_relation.m_mapping_rate,
                                    m_mapping_comment=source_relation.m_mapping_comment,
                                )

                                reverse = ConceptRelationModel(
                                    concept_id_1=relative_id_2,
                                    concept_id_2=relative_id_1,
                                    relationship_id=reverse_relation_name,
                                    invalid_reason=source_relation.invalid_reason,
                                    m_user_id=source_relation.m_user_id,
                                    m_algo_id=Algo.FROM_OTHER_PROJECTS_MAPPING.value,
                                    m_mapping_rate=source_relation.m_mapping_rate,
                                    m_mapping_comment=source_relation.m_mapping_comment,
                                )

                                relation.upserting()
                                reverse.upserting()

                                JobModel(
                                    relative_id_1, "MAPPING", source_relation.m_user_id
                                ).upserting()  # status = 0  by default
                                JobModel(
                                    relative_id_2, "MAPPING", source_relation.m_user_id
                                ).upserting()  # status = 0  by default

            JobModel.update_mapper_job(
                dest_status=2,
                _id=m_concept_relationship_id,
                job_type=["EXTEND_RELATIONS_TO_OTHER_PROJECT"],
            )


def relations_to_other_projects(vocabulary_ids_list=None, project_ids_list=None):
    """
    bulk relations to transfert
    provide vocabylary without prefix ex : ['ccam'] +/- project from which you want to export [4]
    all the vocabulary will be extend with prefix : ex 'APHP - ccam', 'SNDS - ccam' ...

    Remark : job table only use to synchronise solr!!
    """

    if not project_ids_list:
        project_ids_list = [p.m_project_id for p in ProjectModel.find_all()]

    if not vocabulary_ids_list:
        vocabulary_ids_list = VOCABULARY_TO_LINK

    for voc in vocabulary_ids_list:
        project_name_list = [
            ProjectModel.find_by_id(p).m_project_type_id for p in project_ids_list
        ]
        voc_list = [project + SEPARATOR + voc for project in project_name_list]

        for v in voc_list:
            concepts_id = []
            for v in voc_list:
                concepts_id += [c.concept_id for c in ConceptModel.find_by_voc(v)]

            nb_concept_id = len(concepts_id)
            print("len :", nb_concept_id)

            i = 0
            j = 10

            while i < nb_concept_id:
                bash_concepts_id = concepts_id[i:j]
                for id in bash_concepts_id:
                    newJob = JobModel(id, "EXTEND_RELATIONS_TO_OTHER_PROJECT", 0)
                    newJob.upserting()
                extend_relations_for_one_concept()
                i += 10
                j += 10
