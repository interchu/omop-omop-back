import logging
import os

from asynchron.models.concept import ConceptModel
from asynchron.models.job import JobModel
from asynchron.models.synonym import SynonymModel
from share.macros import ISO_OMOP_LANGUAGES, API_DEAD, Algo

API_DEAD = "All the translation API are dead - Wait"


def update_mapper_job_translation(dest_status, initial_status=[], _id=-1):
    """
    possible to search with id or initial_status
    """
    if _id == -1:
        jobs = JobModel.find_by_initial_status(initial_status, ["BULK_TRANSLATION"])
    else:
        jobs = JobModel.find_by_id([_id], ["BULK_TRANSLATION"])

    for job in jobs:
        job.m_status_id = dest_status
        job.upserting()
    jobs = [(job.m_concept_id, job.m_user_id) for job in jobs]

    return jobs


def translate_all(_id, m_user_id, list_available_algo):
    """
    translate one concept,
    to multiple -> according ISO_OMOP_LANGUAGES
       or one dest if string_dest not empty
    with available_algo list
    """

    # trouver la string via concept
    concept = ConceptModel.find_by_id(_id)
    # enlever du dico la language_source et l'anglais
    if concept:
        # find_language or put to english
        language = concept.m_language_id
        language = language.lower() if language else "en"

        languages = {}
        for key in ISO_OMOP_LANGUAGES:
            languages[key] = ISO_OMOP_LANGUAGES[key]

        # suppress to dest the concept_language
        if language in languages:
            del languages[language]

        for iso, omop_concept_id in languages.items():
            if not SynonymModel.find_synonym(
                concept_id=_id,
                language_concept_id=omop_concept_id,
                m_algo_id=[Algo.AUTOMATIC_TRANS.value, Algo.FROM_OTHER_PROJECTS_TRANS.value],
            ):

                command = "torify python /project/asynchron/bulk_trans/one_trans.py "
                command += ' "' + concept.concept_name + '" '
                command += ' "' + language + '" '
                command += ' "' + iso + '" '
                command += ' "' + " ".join(list_available_algo) + '"'

                logging.warn(command)
                translation = os.popen(command).read()

                if translation == API_DEAD:
                    logging.warn("re-loading tor")
                    os.system("kill -HUP `cat /tmp/PID`")
                    translation = os.popen(command).read()

                logging.warn("%s : %s", iso, translation)
                if translation != API_DEAD and translation != "":
                    new_synonym = SynonymModel(
                        concept_id=_id,
                        concept_synonym_name=translation,
                        language_concept_id=omop_concept_id,
                        m_user_id=m_user_id,
                        m_algo_id=1,  # translationAPI
                    )
                    new_synonym.upserting()
                elif translation == API_DEAD:
                    update_mapper_job_translation(dest_status=3, _id=_id)
                    return

        update_mapper_job_translation(dest_status=2, _id=_id)
        JobModel(_id, "EXTEND_SYNONYMS_TO_OTHER_PROJECT", m_user_id).upserting()
        JobModel(_id, "INDEXATION", m_user_id).upserting()


def synonym_translation_api_extend(list_available_algo):
    result = update_mapper_job_translation(initial_status=[0, 1, 3], dest_status=1)

    if len(result) > 0:
        for _id, m_user_id in result:
            translate_all(_id, m_user_id, list_available_algo)
