from app.db import db


class JobModel(db.Model):
    __tablename__ = "mapper_job"
    __table_args__ = {"schema": "susana"}

    id = db.Column("m_job_id", db.Integer, primary_key=True, autoincrement=True)

    m_concept_id = db.Column(db.Integer, nullable=False)
    m_job_type_id = db.Column(db.String(255), nullable=False)
    m_status_id = db.Column(db.Integer, nullable=False)
    m_user_id = db.Column(db.Integer, nullable=False)

    def __init__(self, m_concept_id, m_job_type_id, m_user_id, m_status_id=0):
        self.m_concept_id = m_concept_id
        self.m_job_type_id = m_job_type_id
        self.m_status_id = m_status_id
        self.m_user_id = m_user_id

    def upserting(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_ids(cls, m_job_type_id, m_status_id):
        return cls.query.filter_by(
            m_job_type_id=m_job_type_id, m_status_id=m_status_id
        ).all()
