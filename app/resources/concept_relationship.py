import json
from datetime import datetime

from flask import request
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, reqparse

from app.models.concept import ConceptRelationModel, RelationshipModel, RoleModel
from app.models.job import JobModel
from app.models.synonym import SynonymModel
from conf_files.config import SOLR_AUTH, SOLR_CORE, SOLR_HOST, SOLR_PORT
from pysolrwrapper.core import SolrQuery
from share.macros import Algo, Language


class ConceptRelationList(Resource):
    #    parser = reqparse.RequestParser()
    #    parser.add_argument('mini', type=int, default=2000000000)
    #    parser.add_argument('maxi', type=int)
    #    parser.add_argument('available', type=str,default='true')
    #    parser.add_argument('project_ids', action='append', default=[])
    #    parser.add_argument('relationship_ids', action='append', default=['Maps to'])
    #    parser.add_argument('order_by', type=str, default='desc')
    #    parser.add_argument('page_shown', type=int, default=0)
    #    parser.add_argument('limit', type=int, default=15)

    @classmethod
    @jwt_required
    def get(cls):
        """
        return all the infos about two concepts linked
        concept_id, concept_name, domain_id, vocabulary_id (x2)
        relationship_id
        project_id
        vote
        """
        data = request.args.get("json")
        data = json.loads(data)
        #        data = cls.parser.parse_args()

        mini = data.get("mini", 2000000000)
        maxi = data.get("maxi", None)
        order_by = data.get("order_by", "desc")
        page_shown = data.get("page_shown", 0)
        limit = data.get("limit", 15)
        available = data.get("available", "active")
        project_ids = data.get("project_ids", [])
        relationship_ids = data.get("relationship_ids", ["Maps to"])
        print("data =", data)

        # review only allowed
        if project_ids:
            project_ids = [
                project_id.upper()
                for project_id in project_ids
                if project_id
                in RoleModel.projects_type_id_allowed(get_jwt_identity(), "VOTE")
            ]
        else:
            project_ids = RoleModel.projects_type_id_allowed(get_jwt_identity(), "VOTE")
        if not project_ids:
            return {"Message": "Unauthorized"}, 401

        relations, page_shown, page_found, count = ConceptRelationModel.find_all(
            mini,
            maxi,
            available,
            project_ids,
            relationship_ids,
            order_by,
            page_shown,
            limit,
        )
        return {
            "relations": [x.json() for x in relations],
            "num_found": count,
            "page_shown": page_shown,
            "page_found": page_found,
        }, 200


class ConceptRelation(Resource):
    """
    endpoint : /concept_relationship/<int:_id>
    put does not exist because all the important informations are in the PK!
    """

    parser = reqparse.RequestParser()
    parser.add_argument("concept_id_2", type=int)
    parser.add_argument("relationship_id", type=str)
    parser.add_argument("valid_end_date", type=str)
    parser.add_argument(
        "m_algo_id", type=int, default=Algo.MANUAL_EVALUATION_MAPPING.value
    )  # Manual check relation
    parser.add_argument("m_mapping_comment", type=str, default="")
    parser.add_argument("m_mapping_rate", type=int, default=0)
    parser.add_argument("helper", type=str)

    @classmethod
    @jwt_required
    def get(cls, _id):
        """
        As concept_id_2 and relationship_id are not required, it is possible to
        return many relations
        """
        data = cls.parser.parse_args()
        relations = ConceptRelationModel.find_by_id(
            _id, data["concept_id_2"], data["relationship_id"]
        )

        if relations:
            return {"relation": [x.json() for x in relations]}, 200
        return {"message": "Relationship not found"}, 404

    @classmethod
    @jwt_required
    def put(cls, _id):
        data = cls.parser.parse_args()
        print(data)
        data["m_user_id"] = get_jwt_identity()

        if (
            data.get("concept_id_2", None) == None
            or data.get("relationship_id", None) == None
        ):
            return {
                "message": "concept_id_1, concept_id_2 and relationship_id are required"
            }, 401

        if not RoleModel.write(_id, data["m_user_id"]) or not RoleModel.write(
            data["concept_id_2"], data["m_user_id"]
        ):
            return {"Message": "Unauthorized"}, 401

        # no versioning here only check if there is a new helper
        if data["helper"] and not SynonymModel.find_synonym(
            _id,
            concept_synonym_name=data["helper"],
            language_concept_id=Language.ENGLISH.value,
        ):
            new_synonym = SynonymModel(
                _id, data["helper"], data["m_user_id"], Algo.MANUAL_TRANS.value
            )
            new_synonym.upserting()
        if "helper" in data:
            del data["helper"]

        # find_by_id return a iterable order by desc m_modif_start_datetime
        relations = ConceptRelationModel.find_by_id(
            _id, data["concept_id_2"], data["relationship_id"]
        )
        reverse_relation_name = RelationshipModel.find_by_id(
            data["relationship_id"]
        ).reverse_relationship_id

        if relations:

            print("relations", relations)
            print(
                "there is relatiosn", _id, data["concept_id_2"], data["relationship_id"]
            )
            relations[0].m_modif_end_datetime = datetime.now()

            reverse = ConceptRelationModel.find_by_id(
                data["concept_id_2"], _id, reverse_relation_name
            )
            if reverse:
                reverse[0].m_modif_end_datetime = datetime.now()

        # new relation ('Maps to') and its reverse one ('Mapped from')
        relation = ConceptRelationModel(_id, **data)
        reverse = ConceptRelationModel(_id, **data)
        reverse.concept_id_1, reverse.concept_id_2 = (
            reverse.concept_id_2,
            reverse.concept_id_1,
        )
        reverse.relationship_id = reverse_relation_name
        relation.upserting()
        reverse.upserting()

        # update relationship
        JobModel(_id, "MAPPING", data["m_user_id"]).upserting()  # status = 0  by default
        JobModel(
            data["concept_id_2"], "MAPPING", data["m_user_id"]
        ).upserting()  # status = 0  by default
        # extend relationship
        JobModel(
            relation.m_concept_relationship_id,
            "EXTEND_RELATIONS_TO_OTHER_PROJECT",
            data["m_user_id"],
        ).upserting()  # status = 0  by default

        # solr atomic update
        solr = SolrQuery(host=SOLR_HOST, port=SOLR_PORT, core=SOLR_CORE, auth=SOLR_AUTH)
        solr.set_is_mapped(_id, is_standard=True)
        solr.set_is_mapped(data["concept_id_2"], is_standard=True)

        ret = relation.json()
        ret[
            "help"
        ] = "An item with relationship between concept_id = {} and concept_id = {} were inserted".format(
            _id, data["concept_id_2"]
        )
        return ret, 201  # 201 has been created

    @classmethod
    @jwt_required
    def delete(cls, _id):
        data = cls.parser.parse_args()
        data["m_user_id"] = get_jwt_identity()

        if "helper" in data:
            del data["helper"]

        if (
            data.get("concept_id_2", None) == None
            or data.get("relationship_id", None) == None
        ):
            return {
                "message": "concept_id_1, concept_id_2 and relationship_id are required"
            }, 401

        if not RoleModel.delete(_id, data["m_user_id"]) or not RoleModel.delete(
            data["concept_id_2"], data["m_user_id"]
        ):
            return {"Message": "Unauthorized"}, 401

        # find_by_id return a iterable order by desc m_modif_start_datetime
        relations = ConceptRelationModel.find_by_id(
            _id, data["concept_id_2"], data["relationship_id"]
        )
        reverse_relation_name = RelationshipModel.find_by_id(
            data["relationship_id"]
        ).reverse_relationship_id

        # only an relation created by a user in omop-mapper.fgh.ovh can be deleted
        if relations[0].m_user_id is not None:
            if relations:
                relations[0].m_modif_end_datetime = datetime.now()

                reverse = ConceptRelationModel.find_by_id(
                    data["concept_id_2"], _id, reverse_relation_name
                )
                if reverse:
                    reverse[0].m_modif_end_datetime = datetime.now()

            # new relation ('Maps to') and its reverse one ('Mapped from')
            relation = ConceptRelationModel(_id, **data)
            reverse = ConceptRelationModel(_id, **data)
            reverse.concept_id_1, reverse.concept_id_2 = (
                reverse.concept_id_2,
                reverse.concept_id_1,
            )
            reverse.relationship_id = reverse_relation_name
            relation.invalid_reason = "D"
            reverse.invalid_reason = "D"
            relation.upserting()
            reverse.upserting()
            # update relationship

            # update relationship
            JobModel(
                _id, "MAPPING", data["m_user_id"]
            ).upserting()  # status = 0  by default
            JobModel(
                data["concept_id_2"], "MAPPING", data["m_user_id"]
            ).upserting()  # status = 0  by default
            # extend relationship
            JobModel(
                relation.m_concept_relationship_id,
                "EXTEND_RELATIONS_TO_OTHER_PROJECT",
                data["m_user_id"],
            ).upserting()  # status = 0  by default

            # solr atomic update
            solr = SolrQuery(
                host=SOLR_HOST, port=SOLR_PORT, core=SOLR_CORE, auth=SOLR_AUTH
            )
            solr.unset_is_mapped(_id)
            solr.unset_is_mapped(data["concept_id_2"])

            ret = relation.json()
            ret[
                "help"
            ] = "Relationship with concept_id_1 = {}, concept_id_2 = {} and relationship_id = {} was deleted (invalid_reason = 'D')".format(
                _id, data["concept_id_2"], data["relationship_id"]
            )
            return ret, 201  # 201 has been created

        return {"help": "you can't delete a relation created by OMOP"}, 400
