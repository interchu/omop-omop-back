from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, reqparse

from app.models.concept import ProjectModel, RoleModel
from app.models.user import UserModel
from share.macros import ROLES


class ProjectList(Resource):
    # @jwt_required
    @classmethod
    def get(cls):
        """
        Get all the valid project_id project_type_id
        """
        projects = ProjectModel.valid_projects()
        roles = ROLES
        projects_id = [project.m_project_id for project in projects]
        projects_name = [project.m_project_type_id for project in projects]
        projects_roles_name = []
        for project in projects_name:
            projects_roles_name += [project + "-" + role for role in ROLES]
        return {
            "projects_id": projects_id,
            "projects_name": projects_name,
            "projects_roles_name": projects_roles_name,
        }, 200


class Project(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("m_project_role[]", type=str, action="append")

    @classmethod
    def check_role_project(cls, roles_to_add):
        ret = []
        if ProjectModel.valid_projects() and ROLES:
            valid_project_type_id = [
                project.m_project_type_id for project in ProjectModel.valid_projects()
            ]
            for role in roles_to_add:
                if (
                    role.split("-")[0] in valid_project_type_id
                    and role.split("-")[1] in ROLES
                ):
                    ret.append(role)
                else:
                    print("Not present")
            return ret

    @classmethod
    @jwt_required
    def put(cls, m_username):
        """
        Change roles for user, PUT and DELETE in one!
        Remark : No versioning for roles
        """
        user_jwt = UserModel.find_by_id(get_jwt_identity())
        user_action = UserModel.find_by_username(m_username)
        data = cls.parser.parse_args()

        if user_jwt and user_jwt.m_is_admin == 1 and user_action:
            roles_to_add = (
                data["m_project_role[]"] if data["m_project_role[]"] else []
            )  # list de string
            roles_to_add = cls.check_role_project(roles_to_add)

            # find roles for user_action
            roles_already = RoleModel.find_roles_by_username(m_username)
            roles_already_string = [
                already.project.m_project_type_id + "-" + already.m_role_id
                for already in roles_already
            ]

            # if role has already existed but not in request => delete
            i = 0
            roles_to_delete = []
            for role in roles_already_string:
                if role not in roles_to_add:
                    roles_to_delete.append(roles_already[i])
                i += 1
            for d in roles_to_delete:
                d.delete_from_db()

            # if role hasn't relation exist but in request => add
            roles_to_add = [
                role for role in roles_to_add if role not in roles_already_string
            ]
            for to_add in roles_to_add:
                m_project_id = ProjectModel.find_by_name(
                    to_add.split("-")[0]
                ).m_project_id
                new_role = RoleModel(m_project_id, user_action.id, to_add.split("-")[1])
                new_role.upserting()
            return {"message": "Change role for " + user_action.m_username}, 200
