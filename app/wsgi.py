from app.db import db
from app.server import app

db.init_app(app)

if __name__ == "__main__":
    app.run(port=5000, debug=True)
    app.config["CORS_HEADERS"] = "Content-Type"
