# Susana 0.7.1

Susana is a terminology web tool wich allows importing, searching,
translating, mapping and exporting multilingual medical concepts.

Susana takes its roots within
[usagi](https://www.ohdsi.org/web/wiki/doku.php?id=documentation:software:usagi)
and [athena](https://athena.ohdsi.org/search-terms/start), both OHDSI tools.

Behind the scene, Susana is based on the OMOP Common Data Model which
is a bit extended to allow project, user and security management.

## Documentation

[Read The Doc](https://interhop.frama.io/omop/susana/)

## Quickstart

### Start the app

``` bash
git clone git@framagit.org:interhop/omop/susana.git
git submodule update --init --recursive
make init
docker-compose up

# checkout http://localhost
# user: admin
# password: toto
```

### Use the cli

```bash
$ pip install -e .

$ susana --help
Usage: susana [OPTIONS] COMMAND [ARGS]...

  Susana Command Line Interpreter.

  NOTE: This assumes postgres connection specified into .env

Options:
  --help  Show this message and exit.

Commands:
  load     Load concepts, relationship or synonyms
  upgrade  Upgrade the susana concepts with a new athena export

```

### Load concepts

This loads the FHIR sample terminologies and mapping
```bash
$ susana load --help
Usage: susana load [OPTIONS]

  Load concepts, relationship or synonyms

Options:
  --project-type TEXT             Project acronym. Can be a new or existing
                                  one  [required]

  --vocabulary TEXT               Vocabulary name  [required]
  --domain TEXT                   Domain name  [required]
  --user-id INTEGER               User id of the uploader user  [required]
  --language [EN|FR]              Language of the concepts  [required]
  --concept-path PATH             Relative or absolute path of concept CSV
                                  [required]

  --relationship-path PATH        Relative or absolute path of
                                  concept_relationship CSV  [required]

  --refresh [ALL|CONCEPT|RELATIONSHIP]
                                  Use only to refresh existing concepts
  --debug / --no-debug            Show debug trace  [default: False]
  --help                          Show this message and exit.



$ susana load
 --project-type "FHIR" \
 --user-id 1 \
 --vocabulary "FHIR - data-absent-reason" \
 --domain "Procedure" \
 --langage "EN" \
 --concept-path "resources/postgresql/loader/FHIR_data-absent-reason_concept.csv" \
 --relationship-path "resources/postgresql/loader/FHIR_data-absent-reason_relationship.csv" \
 --debug
```

### Upgrade Athena Concepts

Download and unzip the athena concept in `<a given folder>`.

```bash
$ susana upgrade --help
Usage: susana upgrade [OPTIONS]

  Upgrade the susana concepts with a new athena export

Options:
  --athena-folder PATH  Folder where athena download have been unzipped
                        [required]

  --help                Show this message and exit.


$ susana upgrade --athena-folder "<a given folder>"
```

### Export Concepts by project

```bash
susana export --help
Usage: susana export [OPTIONS]

  Upgrade the susana concepts with a new athena export

Options:
  --export-folder PATH  Folder where csv are exported  [required]
  --project TEXT        Project to export  [required]
  --help                Show this message and exit.

```
