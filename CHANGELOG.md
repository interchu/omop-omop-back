v0.7.1 - 2021-12-10
- Bugfixes: Update frontend documentation link ([#17](https://framagit.org/interhop/omop/susana/-/issues/17))


v0.7.0 - 2021-10-02
- Features: Add export command line fonctionality ([#16](https://framagit.org/interhop/omop/susana/-/issues/16))


v0.6.2 - 2021-09-28
- Bugfixes: Fix versioning gap ([#15](https://framagit.org/interhop/omop/susana/-/issues/15))


v0.6.0 - 2021-09-27
- Features: Add e-mail jobs ([#14](https://framagit.org/interhop/omop/susana/-/issues/14))


v0.5.1 - 2021-09-25
- Features: Add systemd unit ([#13](https://framagit.org/interhop/omop/susana/-/issues/13))


v0.5.0 - 2021-09-25
- Features: Add language translation ([#12](https://framagit.org/interhop/omop/susana/-/issues/12))


v0.4.0 - 2021-08-19
- Features: Provide athena concept updater command line ([#9](https://framagit.org/interhop/omop/susana/-/issues/9))
- Features: Provide susana cli bash command ([#11](https://framagit.org/interhop/omop/susana/-/issues/11))
- Deprecations and Removals: Rationalize integration tests with docker-compose ([#10](https://framagit.org/interhop/omop/susana/-/issues/10))


v0.3.2 - 2021-07-10
- Features: Add a pre-commit hook ([#8](https://framagit.org/interhop/omop/susana/-/issues/8))


v0.3.1 - 2021-06-19
- Features: Add changelog based on towncrier ([#7](https://framagit.org/interhop/omop/susana/-/issues/7))
